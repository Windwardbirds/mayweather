import React from "react";
import "babel-polyfill";
import SearchBar from "./SearchBar";
import WeatherTabs from "./WeatherTabs";
import Loading from "./Loading";

const APPID_KEY = "a0d34f63ea80917270cf119017084c57";

class App extends React.Component {
  state = {
    weatherData: {},
    forecast: {},
    loading: false,
    gotData: false,
    error: ""
  };

  getWeather = async e => {
    e.preventDefault();
    this.setState({
      loading: true,
      error: ""
    });
    const locationName = e.target.elements.locationName.value;
    const apiCall = await fetch(
      `http://api.openweathermap.org/data/2.5/weather?q=${locationName}&APPID=${APPID_KEY}`
    );
    const data = await apiCall.json();
    if (!apiCall.ok) {
      this.setState({
        loading: false,
        gotData: false,
        weatherData: {},
        error: data.message
      });
      return;
    }
    const apiCall2 = await fetch(
      `http://api.openweathermap.org/data/2.5/forecast?q=${locationName}&APPID=${APPID_KEY}`
    );
    const data2 = await apiCall2.json();

    this.setState({
      weatherData: data,
      forecast: data2,
      loading: false,
      gotData: true
    });
  };

  render() {
    const { weatherData, forecast, gotData, loading, error } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-pretitle">Welcome to</h1>
          <h2 className="App-title">MayWeather!</h2>
        </header>
        <SearchBar getWeather={this.getWeather} />
        <h3 className="app-error">{error}</h3>
        {loading ? (
          <Loading />
        ) : (
          gotData && (
            <WeatherTabs weatherData={weatherData} forecast={forecast} />
          )
        )}
      </div>
    );
  }
}

export default App;
