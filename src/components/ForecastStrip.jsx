import React from "react";
import Moment from "react-moment";
import ForecastCard from "./ForecastCard";
import Details from "./Details";

function calculaTiempo(props) {
  const forecast = props;
  const forecastByDay = {};

  forecast.list.forEach(item => {
    const date = new Date(item.dt * 1000);
    const day = `0${date.getDate()}`.slice(-2);
    const month = `0${date.getMonth() + 1}`.slice(-2);
    const year = date.getFullYear();
    const thisDay = `${year}-${month}-${day}`;
    if (!forecastByDay[thisDay]) {
      forecastByDay[thisDay] = [];
    }
    forecastByDay[thisDay].push(item);
  });
  return forecastByDay;
}

class ForecastStrip extends React.Component {
  state = {
    selected: -1,
    selectedDay: [],
    listOfDays: {},
    loading: true
  };

  componentDidMount() {
    const { forecast } = this.props;
    const getList = calculaTiempo(forecast);
    this.setState({
      listOfDays: getList,
      loading: false
    });
  }

  handleClick = id => {
    const { listOfDays } = this.state;
    const head = listOfDays[id];
    this.setState({
      selected: id,
      selectedDay: head
    });
  };

  render() {
    const { selected, loading, listOfDays, selectedDay } = this.state;
    const displayDays = Object.keys(listOfDays).map((key, idx) => {
      return (
        idx < 5 && (
          <ForecastCard
            key={key}
            id={key}
            item={listOfDays[key]}
            handleClick={this.handleClick}
          />
        )
      );
    });
    return (
      <div>
        <div>
          <div className="forecast-strip">{!loading && displayDays}</div>
          {selected !== -1 && (
            <div>
              <h3>
                <h4 className="forecast-title">Forecast Details for: </h4>
                <Moment unix format="dddd DD/MM">
                  {selectedDay[0].dt}
                </Moment>
              </h3>
              <div className="details-strip">
                {selectedDay.map(item => {
                  return <Details key={item.dt} dayData={item} />;
                })}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default ForecastStrip;
