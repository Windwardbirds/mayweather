import React from "react";
import Moment from "react-moment";

const ForecastCard = props => {
  const { item } = props;
  const { icon } = item[0].weather[0];
  const iconURL = `http://openweathermap.org/img/w/${icon}.png`;
  const max = Math.max(
    ...item.map(o => {
      return o.main.temp_max;
    })
  );
  const min = Math.min(
    ...item.map(o => {
      return o.main.temp_min;
    })
  );
  return (
    <div className="forecast-card" onClick={() => props.handleClick(props.id)}>
      <h2 className="card-dayName">
        <strong>
          <Moment unix format="ddd">
            {item[0].dt}
          </Moment>
        </strong>
      </h2>
      <h3>
        <Moment unix format="DD/MM" withTitle>
          {item[0].dt}
        </Moment>
      </h3>
      <h3>
        <img className="weather-icon" src={iconURL} alt="Icon for current weather" />
      </h3>
      <h3 className="temp-max">{` ${Math.round(max - 273.15)} Cº`}</h3>
      <h3 className="temp-min">{` ${Math.round(min - 273.15)} Cº`}</h3>
    </div>
  );
};

export default ForecastCard;
