import React from "react";
import { Tabs, TabList, Tab, TabPanel } from "react-tabs";
import WeatherCard from "./WeatherCard";
import ForecastStrip from "./ForecastStrip";
import "react-tabs/style/react-tabs.css";

class WeatherTabs extends React.Component {
  state = {
    tabIndex: 0
  };

  render() {
    const { weatherData, forecast } = this.props;
    return (
      <Tabs
        selectedIndex={this.state.tabIndex}
        onSelect={tabIndex => this.setState({ tabIndex })}
      >
        <TabList>
          <Tab>Current Weather</Tab>
          <Tab>Forecast Weather</Tab>
        </TabList>
        <TabPanel>
          <section>
            <WeatherCard weatherData={weatherData} />
          </section>
        </TabPanel>
        <TabPanel>
          <section>
            <ForecastStrip forecast={forecast} />
          </section>
        </TabPanel>
      </Tabs>
    );
  }
}

export default WeatherTabs;
