import React from "react";
import Moment from "react-moment";

const Details = props => {
  const { dayData } = props;
  const { icon } = dayData.weather[0];
  const iconURL = `http://openweathermap.org/img/w/${icon}.png`;
  return (
    <div className="details-card">
      <div className="details-row-one">
        <h6>
          <Moment unix format="HH:mm">
            {dayData.dt}
          </Moment>
          -
          <Moment unix format="HH:mm" add={{ hours: 3 }}>
            {dayData.dt}
          </Moment>
        </h6>
        <img src={iconURL} alt="icono de tiempo" />
        <h6>{` ${Math.round(dayData.main.temp - 273.15)} Cº`}</h6>
        <h6>{dayData.weather[0].description}</h6>
      </div>
      <div className="details-row-two">
        <h6>
        <strong>Pressure:</strong>
          {` ${Math.round(dayData.main.pressure)} hpm`}
        </h6>
        <h6>
        <strong>Wind:</strong>
          {` ${dayData.wind.speed} Km/h`}
        </h6>
        <h6>
        <strong>Min Temp:</strong>
          {` ${Math.round(dayData.main.temp_min - 273.15)} Cº`}
        </h6>
        <h6>
        <strong>Humidity:</strong>
          {` ${dayData.main.humidity}  %`}
        </h6>
        <h6>
        <strong>Rain:</strong>
          {` ${dayData.rain ? dayData.rain["3h"] : "0"} mm`}
        </h6>
        <h6>
        <strong>Max Temp:</strong>
          {` ${Math.round(dayData.main.temp_max - 273.15)} Cº`}
        </h6>
      </div>
    </div>
  );
};

export default Details;
