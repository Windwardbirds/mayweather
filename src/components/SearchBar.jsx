import React from "react";

const SearchBar = props => (
  <form onSubmit={props.getWeather}>
    <div>
      <p className="Bar-title">Enter your location.</p>
      <input
        className="Bar-box"
        type="text"
        placeholder="City or City,Country"
        name="locationName"
      />
      <button className="btn btn-outline-info">
        Check Weather
      </button>
    </div>
  </form>
);

export default SearchBar;
