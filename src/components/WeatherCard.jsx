import React from "react";
import Moment from "react-moment";

const WeatherCard = props => {
  const item = props.weatherData;
  const { icon } = item.weather[0];
  const iconURL = `http://openweathermap.org/img/w/${icon}.png`;
  return (
    <div className="card-container">
      <div className="column-one">
        <h5>
          <img
            className="weather-icon"
            src={iconURL}
            alt="Icon for current weather"
          />
        </h5>
        <h5>
          <strong>{` ${Math.round(item.main.temp - 273.15)} Cº`}</strong>
        </h5>
        <h5>{item.weather[0].description}</h5>
      </div>
      <div className="column-two">
        <h5>
          <strong>Pressure:</strong>
          {` ${Math.round(item.main.pressure)} hpm`}
        </h5>
        <h5>
          <strong>Min Temp:</strong>
          {` ${Math.round(item.main.temp_min - 273.15)} Cº`}
        </h5>
        <h5>
          <strong>Sunrise:</strong>
          <Moment unix format="HH:mm">
            {item.sys.sunrise}
          </Moment>
        </h5>
        <h5>
          <strong>Wind:</strong>
          {` ${item.wind.speed} Km/h`}
        </h5>
      </div>
      <div className="column-three">
        <h5>
          <strong>Humidity:</strong>
          {` ${item.main.humidity} %`}
        </h5>
        <h5>
          <strong>Max Temp:</strong>
          {` ${Math.round(item.main.temp_max - 273.15)} Cº`}
        </h5>
        <h5>
          <strong>Sunset:</strong>
          <Moment unix format="HH:mm">
            {item.sys.sunset}
          </Moment>
        </h5>
      </div>
    </div>
  );
};

export default WeatherCard;
