# Lab 3 - Proyecto MayWeather

## Instalación

Una sola vez:

    ```
    npm install --dev
    npm install @babel/plugin-proposal-class-properties --save-dev
    ```

y .babelrc tiene que quedar asi:

{
    "presets": [
        "@babel/preset-env",
        "@babel/preset-react"
    ],
    "plugins": [
        [
          "@babel/plugin-proposal-class-properties"
        ]
    ]
}

    ```
    npm install babel-polyfill
    ```

Para que funcione los css importados:

    ```
    npm install css-loader style-loader --save-dev
    ```

En webpack.config.js cambiar:
    use: ['css-loader']
por:
    use: ['style-loader','css-loader']

    ```
    npm install --save moment react-moment
    ```
Para instalar Tabs(<https://github.com/reactjs/react-tabs>):

    ```
    npm install --save react-tabs
    ```
Para iniciar la App:

    ```
    npm start
    ```

###Decisiones de Diseño

App se encarga de realizar las llamadas a la API.

El componente SearchBar recibe como prop una metodo getWeather que se ejecuta tras un evento onSubmit.

La funcion getWeather se encarga de realizar las llamadas a la api, cargar los datos en el estado de la clase e informar si recibio algun error de la api. Durante la renderizacion se comprueba el estado de la solicitud mediante loading y gotData. Se usa renderizacion condicional con operador ternario para comprobar si esta esperando una respuesta de la api, y en caso negativo solo muestra los datos cuando estos existen.

WeatherTabs se encarga de manejar las tabs y cargar el componente correspondiente pasando los props requeridos. La implementacion de tabs requiere un componente con estado (es decir, una clase).

El componente de clase ForecastStrip al montarse en pantalla usa el metodo de lifecycle ComponentDidMount para obtener y cargar en su estado listOfDays, un objeto donde se guarda la lista de pronosticos de cada dia indexado por fecha. Esta tarea es implementada por la funcion calculaTiempo.

En la renderizacion del componente usamos la funcion map sobre las keys del objeto para imprimir las ForecastCards. Esto en conjunto a un condicional que determina la cantidad de tarjetas que se muestran.

ForecastCard es un componente funcional que recibe como prop una funcion handleClick definida en ForecastStrip. La funcion se ejecuta onClick y carga los detalles del dia correspondiente en el estado de ForecastStrip donde se imprime.